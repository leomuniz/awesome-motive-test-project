<?php
/**
 * Awesome Motive Test Project
 *
 * @package           awesome-motive-test-project
 * @author            Léo Muniz
 * @copyright         2020 Léo Muniz for Awesome Motive
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Awesome Motive Test Project
 * Plugin URI:        awesomemotive.leomuniz.com.br
 * Description:       WordPress plugin to fetch data from an API and display using a shortcode.
 * Version:           1.0.0
 * Author:            Léo Muniz
 * Author URI:        leomuniz.com.br
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       awesome-motive-test-project
 * Domain Path:       /assets/languages
 */

namespace AwesomeMotiveTestProject;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Constants.
define( __NAMESPACE__ . '\PLUGIN_VER', '1.0.1' );
define( __NAMESPACE__ . '\PLUGIN_DIR', __DIR__ );
define( __NAMESPACE__ . '\PLUGIN_URL', plugin_dir_url( __FILE__ ) );
require_once 'includes/constants.php';

$autoloader = require_once 'includes/autoload.php';
$autoloader();

Core\Plugin::get_instance();
