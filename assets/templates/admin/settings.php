<div class='amtp-admin-header'>
	<div class='logo'>
		<img src="<?php echo esc_url( AwesomeMotiveTestProject\IMG_URL . 'awesome-motive-logo-dark.svg' ); ?>" alt="Awesome Motive">    
	</div>
	<h1>Awesome Motive</h1>
	<h4>Test Project Plugin (AMTP)</h4>
</div>

<div class='amtp-admin-menu'>
	<ul class="menu-list">
		<li class="active"><?php esc_html_e( 'Settings', 'awesome-motive-test-project' ); ?></li>
	</ul>
</div>


<div class='amtp-admin-content'>

	<div class='actions'>
		<input type="hidden" id="_amtp_erase_cache_wpnonce" value="<?php echo esc_attr( $vars['_erase_cache_wp_nonce'] ); ?>" />
		<button id="amtp-refresh-data-button" type="submit" class="amtp-admin-button"><?php esc_html_e( 'Refresh data', 'awesome-motive-test-project' ); ?></button>
	<div>

	<?php echo do_shortcode( '[amtp_display_content]' ); ?>

</div>
