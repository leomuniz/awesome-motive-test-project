(function( $ ) {

	$(
		function() {
			if ( typeof ajaxurl === "undefined" ) {
				ajaxurl = ajax_var.ajax_url;
			}

			load_content();
		}
	);

})( jQuery );


function load_content() {

	var $ = jQuery;

	if ( $( '.amtp-content' ).length ) {

		$( '.amtp-loading-text' ).html( msgs.loading_msg );
		$( '.amtp-loading-tag' ).show();
		$( '.amtp-content-container' ).html( '' );

		request_data = {
			'action'         : 'amtp_get_data',
			'_security_code' : $( '#_amtp_wpnonce' ).val()
		};

		$.ajax(
			{
				url      : ajaxurl,
				data     : request_data,
				dataType : 'json',
				method   : 'post',
				success  : function( response, textStatus, jqXHR ) {

					data = response.data;

					thead  = '<thead>';
					thead += '	<tr>';
					data.headers.forEach(
						function( name ) {
							thead += '		<th>' + name + '</th>';
						}
					);
					thead += '	</tr>';
					thead += '<thead>';

					tbody = '<tbody>';
					for ( var key in data.rows ) {
						row    = data.rows[key];
						tbody += '	<tr>';
						tbody += '	<td class="amtp_row_id">' + row.id + '</td>';
						tbody += '	<td class="amtp_row_fname">' + row.fname + '</td>';
						tbody += '	<td class="amtp_row_lname">' + row.lname + '</td>';
						tbody += '	<td class="amtp_row_email">' + row.email + '</td>';
						tbody += '	<td class="amtp_row_date">' + row.date + '</td>';
						tbody += '	</tr>';
					}
					tbody += '<tbody>';

					table  = '<table>';
					table += thead + tbody;
					table += '</table>';

					$( '.amtp-loading-tag' ).hide();
					$( '.amtp-content-container' ).html( table );

				},
				error    : function ( jqXHR, textStatus ) {
					$( '.amtp-loading-tag' ).hide();
					$( '.amtp-content-container' ).html( '<p class="amtp-error"><span class="dashicons dashicons-no"></span> ' + msgs.error_msg + '</p>' );
				}
			}
		);
	}
}
