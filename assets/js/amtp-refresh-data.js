(function( $ ) {

	$(
		function() {

			$( '#amtp-refresh-data-button' ).click(
				function() {

					$( '.amtp-loading-text' ).html( msgs.erase_cache_msg );
					$( '.amtp-loading-tag' ).show();
					$( '.amtp-content-container' ).html( '' );

					request_data = {
						'action'         : 'amtp_erase_data_cache',
						'_security_code' : $( '#_amtp_erase_cache_wpnonce' ).val()
					};

					$.ajax(
						{
							url      : ajaxurl,
							data     : request_data,
							dataType : 'json',
							method   : 'post',
							success  : function ( response, textStatus, jqXHR ) {

								if ( response ) {
									load_content(); // in amtp-content.js file.
								} else {
									$( '.amtp-loading-tag' ).hide();
									$( '.amtp-content-container' ).html( '<p class="amtp-error"><span class="dashicons dashicons-no"></span>' + msgs.error_cache_msg + '</p>' );
								}

							},
							error    : function ( jqXHR, textStatus ) {
								$( '.amtp-loading-tag' ).hide();
								$( '.amtp-content-container' ).html( '<p class="amtp-error"><span class="dashicons dashicons-no"></span>' + msgs.error_cache_msg + '</p>' );
							}
						}
					);
				}
			);

		}
	);

})( jQuery );
