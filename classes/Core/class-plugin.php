<?php

namespace AwesomeMotiveTestProject\Core;

use AwesomeMotiveTestProject as amtp;


/**
 * Class Plugin.
 * Basic stuff for plugin
 *
 * @since 1.0.0
 */
class Plugin {

	/**
	 * Holds the plugin instance
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private static $instance;


	/**
	 * Singleton getInstance to avoid multiple instances
	 *
	 * @since 1.0.0
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	/**
	 * Private __construct class
	 *
	 * @since 1.0.0
	 */
	private function __construct() {
		$this->init();
	}


	/**
	 * Initialize hooks (actions, filters), shortcodes and WP CLI commands
	 *
	 * @since 1.0.0
	 */
	public function init() {

		// Enqueue Scripts and Styles.
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

		// shortcodes declarations.
		add_shortcode( 'amtp_display_content', array( 'AwesomeMotiveTestProject\Frontend\Shortcodes', 'amtp_display_content' ) );

		// AJAX resquests.
		add_action( 'wp_ajax_amtp_get_data', array( __NAMESPACE__ . '\Ajax', 'amtp_get_data' ) );
		add_action( 'wp_ajax_nopriv_amtp_get_data', array( __NAMESPACE__ . '\Ajax', 'amtp_get_data' ) );
		add_action( 'wp_ajax_amtp_erase_data_cache', array( __NAMESPACE__ . '\Ajax', 'amtp_erase_data_cache' ) );

		// Admin MENU Item.
		if ( is_admin() ) {
			add_action( 'admin_menu', array( 'AwesomeMotiveTestProject\Admin\Controller', 'create_menu_item' ) );
		}

		// WP CLI commands.
		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			\WP_CLI::add_command( 'amtp', __NAMESPACE__ . '\WPCLI' );
		}
	}


	/**
	 * Enqueue scripts and styles for admin screen
	 *
	 * @since 1.0.0
	 */
	public function admin_enqueue_scripts() {

		wp_enqueue_style( 'awesome-motive-test-project-css', amtp\CSS_URL . 'amtp-admin.css', array(), amtp\PLUGIN_VER, 'all' );
		wp_register_script( 'awesome-motive-test-project-refresh-data-js', amtp\JS_URL . 'amtp-refresh-data.js', array( 'jquery' ), amtp\PLUGIN_VER, false );

		if ( ! wp_script_is( 'awesome-motive-test-project-js', 'registered' ) ) {
			wp_register_script( 'awesome-motive-test-project-js', amtp\JS_URL . 'amtp-content.js', array( 'jquery' ), amtp\PLUGIN_VER, false );
		}
	}


	/**
	 * Enqueue scripts and styles for frontend and creates ajax_url URL
	 *
	 * @since 1.0.0
	 */
	public function wp_enqueue_scripts() {

		\wp_enqueue_style( 'awesome-motive-test-project-css', amtp\CSS_URL . 'amtp-frontend.css', array(), amtp\PLUGIN_VER, 'all' );
		\wp_register_script( 'awesome-motive-test-project-js', amtp\JS_URL . 'amtp-content.js', array( 'jquery' ), amtp\PLUGIN_VER, false );
		\wp_localize_script( 'awesome-motive-test-project-js', 'ajax_var', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}


	/**
	 * Load admin templates
	 *
	 * @since 1.0.0
	 *
	 * @param string $template Template file name to be loaded.
	 * @param array  $vars     Array containing variables to be used in template.
	 */
	public static function load_admin_template( $template, $vars = array() ) {

		$template = ( strpos( $template, '.php' ) !== false ) ? $template : $template . '.php';
		require amtp\ADMIN_TEMPLATES_DIR . $template;

	}

}
