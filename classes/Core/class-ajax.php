<?php

namespace AwesomeMotiveTestProject\Core;

/**
 * Class Ajax
 * AJAX calls class
 *
 * @since 1.0.0
 */
class Ajax {

	/**
	 * Fetch data from controller and send back to browser as JSON content
	 *
	 * @since 1.0.0
	 */
	public static function amtp_get_data() {

		// Check wp nonce.
		check_ajax_referer( 'display_data_shortcode', '_security_code' );

		$controller = new namespace\Controller();

		$data = $controller->get_data();

		wp_send_json( $data );

	}

	/**
	 * Erase transient that stores data as cache for 1 hour.
	 * Send back true/false to browser
	 *
	 * @since 1.0.0
	 */
	public static function amtp_erase_data_cache() {

		// Check wp nonce.
		check_ajax_referer( 'erase_data_cache', '_security_code' );

		$ctrl = new Controller();

		if ( $ctrl->data_cache_exists() ) {
			$deleted = $ctrl->erase_data_cache();
			wp_send_json( $deleted );
		} else {
			wp_send_json( 1 );
		}

	}

}
