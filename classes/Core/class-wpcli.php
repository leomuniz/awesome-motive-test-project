<?php

namespace AwesomeMotiveTestProject\Core;

/**
 * Class WPCLI
 * WP CLI Commands for plugin
 *
 * @since 1.0.0
 */
class WPCLI extends \WP_CLI_Command {

	/**
	 * WP CLI command wp amtp erase_data_cache.
	 * Deletes amtp_data transient to allow fetching data again from the API endpoint.
	 *
	 * @since 1.0.0
	 */
	public function erase_data_cache() {

		echo( PHP_EOL );

		\WP_CLI::log( 'Erasing data cache... ' );

		$ctrl = new Controller();

		if ( $ctrl->data_cache_exists() ) {

			$deleted = $ctrl->erase_data_cache();
			if ( $deleted ) {
				\WP_CLI::success( 'Data cache erased successfully!' );
			} else {
				\WP_CLI::error( 'Could not erase data cache.' );
			}
		} else {
			\WP_CLI::warning( 'There is no data cache to erase' );
		}

		echo( PHP_EOL );

	}

}
