<?php

namespace AwesomeMotiveTestProject\Core;

/**
 * Class Controller
 * Controller to handle data for plugin
 *
 * @since 1.0.0
 */
class Controller {

	/**
	 * Endpoint to retrieve data from
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $data_endpoint = 'https://miusage.com/v1/challenge/1/';


	/**
	 * A wp_option name for storing data cache
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $cache_option_name = 'amtp_data';

	/**
	 * A wp_option name for storing data cache expiration time
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $cache_expiration_option_name = 'amtp_data_expiration';

	/**
	 * Data cache expiration limit in seconds
	 *
	 * @since 1.0.0
	 *
	 * @var integer
	 */
	private $cache_limit = 3600; // 3600 = 1 hour


	/**
	 * Get data from the URL if there is no data in transient amtp_data and creates 1h transient
	 * Replaces timestamps by readable dates and makes table headers tranlatable
	 *
	 * @since 1.0.0
	 */
	public function get_data() {

		$data = $this->get_cached_data();

		if ( empty( (array) $data ) ) {
			$response = wp_remote_get( $this->data_endpoint );

			if ( is_wp_error( $response ) ) {
				return new \stdClass();
			}

			$body = wp_remote_retrieve_body( $response );
			$data = json_decode( $body );

			if ( ( empty( $data->data->headers ) ) || ( empty( $data->data->rows ) ) ) { // if there is no headers or no rows, return an empty stdClass.
				return new \stdClass();
			} else {
				$data = $this->sanitize_api_input( $data );
			}

			// replaces timestamp by readable dates.
			foreach ( $data->data->rows as $key => $values ) {
				$data->data->rows->$key->date = gmdate( 'm/d/Y', $data->data->rows->$key->date );
			}

			$this->create_data_cache( $data );

		}

		// Makes headers translatable.
		if ( ! empty( $data->data->headers ) ) {
			foreach ( $data->data->headers as $key => $value ) {
				switch ( $data->data->headers[ $key ] ) {
					case 'First Name':
						$data->data->headers[ $key ] = __( 'First Name', 'awesome-motive-test-project' );
						break;
					case 'Last Name':
						$data->data->headers[ $key ] = __( 'Last Name', 'awesome-motive-test-project' );
						break;
					case 'Email':
						$data->data->headers[ $key ] = __( 'Email', 'awesome-motive-test-project' );
						break;
					case 'Date':
						$data->data->headers[ $key ] = __( 'Date', 'awesome-motive-test-project' );
						break;
				}
			}
		}

		return $data;

	}



	/**
	 * Stores inputted data into a wp_option and creates another wp_option to control its expiration
	 *
	 * @since 1.0.0
	 *
	 * @param stdClass $data Data from API to be cached.
	 */
	public function create_data_cache( $data ) {

		if ( ! empty( $data ) ) {
			update_option( $this->cache_option_name, $data );
			update_option( $this->cache_expiration_option_name, time() );
		}

	}



	/**
	 * Checks if data has expired and fetches its content from wp_option if it's not expired.
	 *
	 * @since 1.0.0
	 *
	 * @return stdClass object
	 */
	public function get_cached_data() {

		$expire = get_option( $this->cache_expiration_option_name );

		if ( ! empty( $expire ) ) {

			if ( time() > ( $expire + $this->cache_limit ) ) { // cache expired!
				return new \stdClass();
			} else {
				return get_option( $this->cache_option_name );
			}
		} else {
			return new \stdClass();
		}

	}


	/**
	 * Erases both wp options for data and expiration time.
	 * Returns true in case both data and expiration options are deleted.
	 *
	 * @since 1.0.0
	 * @return boolean
	 */
	public function erase_data_cache() {

		$cache_deleted      = delete_option( $this->cache_option_name );
		$expiration_deleted = delete_option( $this->cache_expiration_option_name );

		return $cache_deleted && $expiration_deleted;

	}


	/**
	 * Checks if the wp_option amtp_data exists
	 *
	 * @since 1.0.0
	 * @return boolean
	 */
	public function data_cache_exists() {

		return ! empty( get_option( $this->cache_option_name ) );

	}


	/**
	 * Sanitizes API input data
	 *
	 * @since 1.0.0
	 *
	 * @param stdClass $data Data from API to be sanitized.
	 *
	 * @return boolean
	 */
	public function sanitize_api_input( $data ) {

		// Sanitize Headers as text.
		if ( ! empty( $data->data->headers ) ) {
			foreach ( $data->data->headers as $key => $value ) {
				$data->data->$headers = \sanitize_text_field( $data->data->$headers );
			}
		}

		// Sanitize rows values.
		if ( ! empty( $data->data->rows ) ) {
			foreach ( $data->data->rows as $key => $values ) {
				$data->data->rows->$key->id    = intval( $data->data->rows->$key->id );
				$data->data->rows->$key->fname = \sanitize_text_field( $data->data->rows->$key->fname );
				$data->data->rows->$key->lname = \sanitize_text_field( $data->data->rows->$key->lname );
				$data->data->rows->$key->email = \sanitize_email( $data->data->rows->$key->email );
				$data->data->rows->$key->date  = intval( $data->data->rows->$key->date );
			}
		}

		return $data;
	}

}
