<?php

namespace AwesomeMotiveTestProject\Admin;

use AwesomeMotiveTestProject as amtp;
use AwesomeMotiveTestProject\Core;

/**
 * Class Controller
 * Admin Controller methods
 *
 * @since 1.0.0
 */
class Controller {

	/**
	 * Creates WP menu AMTP item
	 *
	 * @since 1.0.0
	 */
	public static function create_menu_item() {

		\add_menu_page(
			'Awesome Motive Test Project',
			'AMTP',
			'manage_options',
			'amtp',
			array( __NAMESPACE__ . '\Controller', 'display_admin_content' ),
			'dashicons-welcome-widgets-menus',
			97
		);

	}

	/**
	 * Loads template for admin screen
	 *
	 * @since 1.0.0
	 */
	public static function display_admin_content() {

		$vars                          = array();
		$vars['_erase_cache_wp_nonce'] = wp_create_nonce( 'erase_data_cache' );

		$amtp_plugin = Core\Plugin::get_instance();
		$amtp_plugin->load_admin_template( 'settings', $vars );
	}
}
