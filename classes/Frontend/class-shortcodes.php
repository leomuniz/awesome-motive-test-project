<?php

namespace AwesomeMotiveTestProject\Frontend;

use AwesomeMotiveTestProject as amtp;

/**
 * Class Shortcodes
 * Shortcodes methods
 *
 * @since 1.0.0
 */
class Shortcodes {

	/**
	 * Shortcode amtp_display_content
	 * Enqueue scripts for this shortcode and returns HTML content to replace the shortcode
	 *
	 * @since 1.0.0
	 * @return string HTML Content
	 */
	public static function amtp_display_content() {

		$msgs                = array();
		$msgs['loading_msg'] = esc_html__( 'Loading content...', 'awesome-motive-test-project' );
		$msgs['error_msg']   = esc_html__( 'Error loading content.', 'awesome-motive-test-project' );

		if ( ! wp_script_is( 'awesome-motive-test-project-js', 'registered' ) ) {
			wp_register_script( 'awesome-motive-test-project-js', amtp\JS_URL . 'amtp-content.js', array( 'jquery' ), amtp\PLUGIN_VER, false );
		}

		\wp_localize_script( 'awesome-motive-test-project-js', 'msgs', $msgs );

		// Only adds the JS file if the shortcode is requested.
		\wp_enqueue_script( 'awesome-motive-test-project-js' );

		if ( \is_admin() ) {
			$msgs['erase_cache_msg'] = esc_html__( 'Erasing cache data...', 'awesome-motive-test-project' );
			$msgs['error_cache_msg'] = esc_html__( 'Error erasing cache data.', 'awesome-motive-test-project' );

			\wp_localize_script( 'awesome-motive-test-project-refresh-data-js', 'msgs', $msgs );
			\wp_enqueue_script( 'awesome-motive-test-project-refresh-data-js' );
		}

		$wp_nonce = wp_create_nonce( 'display_data_shortcode' );

		$loading_gif  = amtp\IMG_URL . 'ajax-loader.gif';
		$loading_img  = '<img class="amtp-loading-gif" src="' . $loading_gif . '" alt="' . esc_html__( 'Loading', 'awesome-motive-test-project' ) . '..." />';
		$loading_text = '<span class="amtp-loading-text"></span>';
		$loading_tag  = '<div class="amtp-loading-tag">' . $loading_img . ' ' . $loading_text . '</div>';
		$content_tag  = '<div class="amtp-content-container"></div>';

		$placeholder_html  = '<p class="amtp-content">' . $loading_tag . ' ' . $content_tag . '</p>';
		$placeholder_html .= '<input type="hidden" id="_amtp_wpnonce" name="_wpnonce" value="' . $wp_nonce . '" />';

		return $placeholder_html;
	}
}
