<?php

namespace AwesomeMotiveTestProject;

define( __NAMESPACE__ . '\CLASSES', namespace\PLUGIN_DIR . '/classes/' );

define( __NAMESPACE__ . '\ASSETS_DIR', namespace\PLUGIN_DIR . '/assets/' );

define( __NAMESPACE__ . '\ASSETS_URL', namespace\PLUGIN_URL . 'assets/' );

define( __NAMESPACE__ . '\CSS_URL', namespace\ASSETS_URL . 'css/' );

define( __NAMESPACE__ . '\JS_URL', namespace\ASSETS_URL . 'js/' );

define( __NAMESPACE__ . '\IMG_URL', namespace\ASSETS_URL . 'img/' );

define( __NAMESPACE__ . '\ADMIN_TEMPLATES_DIR', namespace\ASSETS_DIR . 'templates/admin/' );

define( __NAMESPACE__ . '\FRONTEND_TEMPLATES_DIR', namespace\ASSETS_DIR . 'templates/frontend/' );

define( __NAMESPACE__ . '\LANGUAGES_DIR', namespace\ASSETS_DIR . 'languages/' );
